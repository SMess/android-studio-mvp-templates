package ${packageName}

import com.smess.basemvpandroidarchitecture.workers.AbstractWorker

class ${className}Worker(private val callback: ${className}WorkerCallback, private val tag: String) : AbstractWorker(){

    override fun run() {
    	//Make async stuff here
    }
}