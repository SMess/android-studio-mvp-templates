## android-studio-mvp-templates

Template for Android Studio, to create a set of files for BaseMvpAndroidArchitecture library's MVP functionality.
* See this repo for more details : https://gitlab.com/SMess/base-mvp-android-architecture

In order to use this in your project. 

* Navigate to the location of the templates folder :

  On Windows/Linux: 
      {ANDROID_STUDIO_LOCATION}/plugins/android/lib/templates/

  On MacOS, this folder was the following:
      /Applications/Android Studio.app/Contents/plugins/android/lib/templates/



  Then, copy/past the folders :

  	  *This is to easelly create features (Activities or Fragments) based on Mvp Pattern*
      MVPEmptyActivity in the "activities/" folder
      MVPEmptyFragment in the "other/" folder
      MVPWorker in the "other/" folder

And feel free to add other templates if needed ! :) 

* Restart Android Studio, and then when you right click on a folder to create a new file, you will be able to see the new option to create a MVP Function in the associated menu (Ex: Activity -> Mvp Empty Activity).

* When upgrading Android Studio, and you have custom groups of file templates in the installation area specified above, Studio will say you have conflicts in the area that need to be deleted. Unfortunately you will have to remove them, and then place them in the same folder after the upgrade


* One your project created, the last step is to copy/past the bash file "MvpAndroidProjectPackages.bash" to the root of the project (in the same level as app folder). 

* Run the script as bellow: Pass the android package name as param (replace dots by "/")  
	  Ex : "bash MvpAndroidProjectPackages.bash com/example/myapp"



## useful links:

https://riggaroo.co.za/custom-file-template-group-android-studiointellij/


https://medium.com/@milenko_52829/how-to-speed-up-development-by-using-mvp-templates-for-android-studio-37174546c1b8

