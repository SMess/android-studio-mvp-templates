#This will generate all empty packages needed by the Mvp Architecture
#Copy/past the file "MvpAndroidProjectPackages.bash" in root folder of the project (in the same level as app folder)
#Run the script as bellow:
#Pass the android package name as param (replace dots by "/"  ex : "bash MvpAndroidProjectPackages.bash com/example/myapp")


mkdir "app/src/integ"
mkdir "app/src/integ/assets"
mkdir "app/src/integ/res"

mkdir "app/src/prod"
mkdir "app/src/prod/assets"
mkdir "app/src/prod/res"

mkdir "app/src/main/java/$1/application"

mkdir "app/src/main/java/$1/domain"
mkdir "app/src/main/java/$1/domain/models"
mkdir "app/src/main/java/$1/domain/workers"

mkdir "app/src/main/java/$1/scenes"
mkdir "app/src/main/java/$1/scenes/main"

mkdir "app/src/main/java/$1/services"
mkdir "app/src/main/java/$1/sp"
mkdir "app/src/main/java/$1/storage"
mkdir "app/src/main/java/$1/utils"

