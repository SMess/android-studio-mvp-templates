<?xml version="1.0"?>
<recipe>
    <#if includeLayout>
        <instantiate from="root/res/layout/fragment_blank.xml.ftl"
                       to="${escapeXmlAttribute(resOut)}/layout/${escapeXmlAttribute(fragmentName)}.xml" />

        <open file="${escapeXmlAttribute(resOut)}/layout/${escapeXmlAttribute(fragmentName)}.xml" />
    </#if>

	<instantiate from="root/src/app_package/Contract.kt.ftl"
                   to="${escapeXmlAttribute(srcOut)}/${className}Contract.kt" />

	<instantiate from="root/src/app_package/MvpView.kt.ftl"
                   to="${escapeXmlAttribute(srcOut)}/${className}Fragment.kt" />

	<instantiate from="root/src/app_package/Presenter.kt.ftl"
                   to="${escapeXmlAttribute(srcOut)}/${className}Presenter.kt" />

	<open file="${srcOut}/${className}Presenter.kt"/>
</recipe>