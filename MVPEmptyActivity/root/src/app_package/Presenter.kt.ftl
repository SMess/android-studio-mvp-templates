package ${packageName}

import com.smess.basemvpandroidarchitecture.ui.BasePresenter
import com.smess.basemvpandroidarchitecture.ui.MVPContract

class ${className}Presenter(viewContract: ${className}Contract.ViewContract) : BasePresenter<MVPContract.ViewContract>(viewContract),
        ${className}Contract.PresenterContract.View {

    companion object {
        private val TAG = ${className}Presenter::class.java.name
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
    }
}