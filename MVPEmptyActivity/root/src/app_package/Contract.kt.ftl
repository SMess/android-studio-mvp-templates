package ${packageName}

import com.smess.basemvpandroidarchitecture.ui.MVPContract


interface ${className}Contract {

    interface ViewContract : MVPContract.ViewContract {
        //functions implemented by the view and called from the presenter goes here 
    }

    interface PresenterContract : MVPContract.PresenterContract {
        //functions implemented by the presenter and called from the view goes here 
    }
}