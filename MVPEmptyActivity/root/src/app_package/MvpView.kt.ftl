package ${packageName}


import android.annotation.SuppressLint
import android.os.Bundle
import com.smess.basemvpandroidarchitecture.ui.BaseActivity
import kotlinx.android.synthetic.main.${layoutName}.*

class ${className}Activity : BaseActivity<${className}Contract.PresenterContract.View>(), ${className}Contract.ViewContract {


    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        <#if generateLayout>
        setContentView(R.layout.${layoutName})
		</#if>
    }

    override fun createPresenter(): ${className}Contract.PresenterContract.View {
        return ${className}Presenter(this)
    }
}
