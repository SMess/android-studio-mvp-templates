<?xml version="1.0"?>
<recipe>
	<#include "../common/recipe_manifest.xml.ftl" />

	<#if generateLayout>
	    <#include "../common/recipe_simple.xml.ftl" />
	    <open file="${escapeXmlAttribute(resOut)}/layout/${layoutName}.xml" />
	</#if>

	<instantiate from="src/app_package/Contract.kt.ftl"
                   to="${escapeXmlAttribute(srcOut)}/${className}Contract.kt" />

	<instantiate from="src/app_package/MvpView.kt.ftl"
                   to="${escapeXmlAttribute(srcOut)}/${className}Activity.kt" />

	<instantiate from="src/app_package/Presenter.kt.ftl"
                   to="${escapeXmlAttribute(srcOut)}/${className}Presenter.kt" />


	<open file="${srcOut}/${className}Presenter.kt"/>
</recipe>